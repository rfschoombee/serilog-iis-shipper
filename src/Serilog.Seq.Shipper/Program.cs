﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog.Seq.Infrastructure;
using Serilog.Seq.Storage;
using System.IO;
using System.Threading.Tasks;

namespace Serilog.Seq.Shipper
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await CreateHostBuilder(args)
            .Build()
            .RunAsync();

            // Important to call at exit so that batched events are flushed.
            Log.CloseAndFlush();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args)
                .ConfigureHostConfiguration(ConfigureHost)
                .ConfigureServices(ConfigureServices)
                .UseSerilog(ConfigureLogging)
                .UseConsoleLifetime();

            return hostBuilder;
        }

        private static void ConfigureHost(IConfigurationBuilder configHost)
        {
            configHost.SetBasePath(Directory.GetCurrentDirectory());
            configHost.AddEnvironmentVariables("APP_");
        }

        private static void ConfigureServices(HostBuilderContext hostContext, IServiceCollection services)
        {
            services.AddOptions();
            services.AddMessageBus();
            services.AddLiteDb(hostContext);
            services.AddIISLogLineParser();
            services.AddIISLogMonitor(hostContext);
        }

        private static void ConfigureLogging(HostBuilderContext hostContext, LoggerConfiguration loggerConfiguration)
        {
            loggerConfiguration.ReadFrom.Configuration(hostContext.Configuration);
        }
    }
}

