using Serilog.Seq.Infrastructure;
using System;
using Tx.Windows;

namespace Serilog.Seq.Shipper
{
    public class IISLogEvent : IMessage
    {
        public Guid Id { get; private set; } = Guid.NewGuid();
        public W3CEvent Content { get; private set; }

        public IISLogEvent(W3CEvent content)
        {
            this.Content = content;
        }
    }
}