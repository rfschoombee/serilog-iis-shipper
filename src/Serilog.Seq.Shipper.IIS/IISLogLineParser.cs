using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Serilog.Seq.Infrastructure;
using Serilog.Seq.IO;
using Serilog.Seq.Storage;

namespace Serilog.Seq.Shipper
{
    public class IISLogLineParser : IHostedService
    {
        private readonly IMessageBus messageBus;
        private readonly IEventCorrelationRepository repository;
        private readonly ILogger logger;
        private SubscriptionToken subscriptionToken;

        public IISLogLineParser(IMessageBus messageBus, IEventCorrelationRepository repository, ILogger logger)
        {
            this.messageBus = messageBus;
            this.repository = repository;
            this.logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            subscriptionToken = messageBus.Subscribe<IISLogEvent>(HandleLogEventAsync);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            messageBus.Unsubscribe(subscriptionToken);
            return Task.CompletedTask;
        }

        private async void HandleLogEventAsync(IISLogEvent @event)
        {
            logger.Debug($"Handling log line event {@event.Id}");

            var line = @event.Content;

            var message = $"{line.cs_host} {line.cs_method} {line.cs_uri_stem} {line.cs_uri_query} {line.s_port} {line.s_ip} {line.sc_status}";

            var eventCorrelation = new EventCorrelation(message);

            logger.Debug($"Event {eventCorrelation.Hash} Parsed: {eventCorrelation.Message}");

            var events = await repository.GetEventsAsync();

            logger.Debug($"Events Returned From Local Storage: {events.Count()}");

            if (events.Any(e => e.Hash == eventCorrelation.Hash))
                return;

            logger.Debug($"Publishing event: {eventCorrelation.Hash}");

            await repository.AddEventAsync(eventCorrelation);

            switch (line.sc_status)
            {
                case HttpStatus.STATUS_200:
                case HttpStatus.STATUS_201:
                    logger.Information("{Host} {Method} {UriStem} {UriQuery} {Port} {IP} {Status}",
                    line.cs_host, line.cs_method, line.cs_uri_stem, line.cs_uri_query, line.s_port, line.s_ip, line.sc_status);
                    break;
                case HttpStatus.STATUS_401:
                case HttpStatus.STATUS_403:
                    logger.Warning("{Host} {Method} {UriStem} {UriQuery} {Port} {IP} {Status}",
                    line.cs_host, line.cs_method, line.cs_uri_stem, line.cs_uri_query, line.s_port, line.s_ip, line.sc_status);
                    break;
                case HttpStatus.STATUS_400:
                case HttpStatus.STATUS_404:
                case HttpStatus.STATUS_500:
                    logger.Error("{Host} {Method} {UriStem} {UriQuery} {Port} {IP} {Status}",
                    line.cs_host, line.cs_method, line.cs_uri_stem, line.cs_uri_query, line.s_port, line.s_ip, line.sc_status);
                    break;
                default:
                    break;
            }

            logger.Debug($"Event published: {eventCorrelation.Hash}");
        }
    }
}