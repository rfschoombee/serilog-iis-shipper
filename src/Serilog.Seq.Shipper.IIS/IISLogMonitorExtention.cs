﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog.Seq.IO;

namespace Serilog.Seq.Shipper
{
    public static class IISLogMonitorExtention
    {
        public static void AddIISLogMonitor(this IServiceCollection services, HostBuilderContext hostContext)
        {
            services.Configure<FileMonitorConfiguration>(hostContext.Configuration.GetSection("FileMonitor"));
            services.AddHostedService<IISLogMonitor>();
        }
    }
}