﻿using Microsoft.Extensions.DependencyInjection;

namespace Serilog.Seq.Shipper
{
    public static class IISLogLineParserExtention
    {
        public static void AddIISLogLineParser(this IServiceCollection services)
        {
            services.AddHostedService<IISLogLineParser>();
        }
    }
}