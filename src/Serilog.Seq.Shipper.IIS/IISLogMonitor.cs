using System.IO;
using System.Linq;
using Microsoft.Extensions.Options;
using Serilog.Seq.Infrastructure;
using Serilog.Seq.IO;
using Tx.Windows;

namespace Serilog.Seq.Shipper
{
    public class IISLogMonitor : FileMonitorBase
    {
        private readonly IMessageBus messageBus;
        private readonly ILogger logger;

        public IISLogMonitor(IMessageBus messageBus, IOptions<FileMonitorConfiguration> options, ILogger logger) : base(options, logger)
        {
            this.messageBus = messageBus;
            this.logger = logger;
        }

        public override async void HandleFileChangesAsync(FileSystemEventArgs e)
        {
            logger.Debug($"File {e.ChangeType}: {e.Name}");
            using (var filestream = new FileStream(e.FullPath, FileMode.Open))
            {
                var events = W3CEnumerable.FromStream(new StreamReader(filestream))
                .OrderByDescending(o => o.dateTime)
                .ToList();

                logger.Debug($"Ordered {events.Count} lines");

                var @event = events.First();

                if (@event == null)
                    return;

                logger.Debug($"New entry found on {@event.dateTime}");

                await messageBus.PublishAsync(new IISLogEvent(@event));
            }
        }
    }
}