namespace Serilog.Seq.IO
{
    public class FileMonitorConfiguration
    {
        public string DirectoryPath { get; set; }
    }
}