using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Serilog.Seq.IO
{
    public abstract class FileMonitorBase : IHostedService
    {
        private readonly FileSystemWatcher filewatcher;
        private readonly IOptions<FileMonitorConfiguration> options;

        public FileMonitorBase(IOptions<FileMonitorConfiguration> options, ILogger logger)
        {
            this.options = options;
            filewatcher = new FileSystemWatcher(this.options.Value.DirectoryPath);
            logger.Debug($"Monitoring file at: {options.Value.DirectoryPath}");
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            filewatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite;
            filewatcher.Changed += OnFileWatchChangedEvent;
            filewatcher.EnableRaisingEvents = true;
            filewatcher.IncludeSubdirectories = true;
            
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            filewatcher.Dispose();
            return Task.CompletedTask;
        }

        private void OnFileWatchChangedEvent(object sender, FileSystemEventArgs e) => HandleFileChangesAsync(e);

        public abstract void HandleFileChangesAsync(FileSystemEventArgs e);
    }
}