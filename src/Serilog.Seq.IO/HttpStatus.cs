﻿namespace Serilog.Seq.IO
{
    public class HttpStatus
    {
        public const string STATUS_200 = "200";
        public const string STATUS_201 = "201";
        public const string STATUS_400 = "400";
        public const string STATUS_401 = "401";
        public const string STATUS_403 = "403";
        public const string STATUS_404 = "404";
        public const string STATUS_500 = "500";
    }
}