﻿namespace Serilog.Seq.Infrastructure
{
    public interface ISubscription
    {
        SubscriptionToken SubscriptionToken { get; }
        void Publish(IMessage message);
    }
}
