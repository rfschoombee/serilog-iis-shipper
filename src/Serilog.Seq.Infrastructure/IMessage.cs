﻿using System;

namespace Serilog.Seq.Infrastructure
{
    public interface IMessage
    {
        Guid Id { get; }
    }
}
