﻿using Microsoft.Extensions.DependencyInjection;

namespace Serilog.Seq.Infrastructure
{
    public static class MessageBusExtensions
    {
        public static void AddMessageBus(this IServiceCollection services)
        {
            services.AddSingleton<IMessageBus, MessageBus>();
        }
    }
}
