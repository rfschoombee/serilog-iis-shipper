using System;
using System.Threading.Tasks;

namespace Serilog.Seq.Infrastructure
{
    public interface IMessageBus
    {
        SubscriptionToken Subscribe<TMessage>(Action<TMessage> messageHandler) where TMessage : IMessage;
        void Unsubscribe(SubscriptionToken Token);
        void Publish<TMessage>(TMessage message) where TMessage : IMessage;
        Task PublishAsync<TMessage>(TMessage message) where TMessage : IMessage;
    }
}