using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Serilog.Seq.Infrastructure
{
    public class MessageBus : IMessageBus
    {
        private readonly object subscriptionsLock = new object();
        private readonly IDictionary<Type, List<ISubscription>> subscriptions;
        private readonly ILogger logger;

        public MessageBus(ILogger logger)
        {
            subscriptions = new Dictionary<Type, List<ISubscription>>();
            this.logger = logger;
        }

        public void Publish<TMessage>(TMessage message) where TMessage : IMessage
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var allSubscriptions = new List<ISubscription>();

            lock (subscriptionsLock)
            {
                if (subscriptions.ContainsKey(typeof(TMessage)))
                    allSubscriptions = subscriptions
                    .Where(o => o.GetType() == typeof(TMessage))
                    .SelectMany(o => o.Value)
                    .ToList();
            }

            try
            {
                allSubscriptions.ForEach(s => s.Publish(message));
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Failed to publish message");
            }
        }

        public Task PublishAsync<TMessage>(TMessage message) where TMessage : IMessage
        {
            return Task.Run(() => Publish(message));
        }

        public SubscriptionToken Subscribe<TMessage>(Action<TMessage> messageHandler) where TMessage : IMessage
        {
            if (messageHandler == null)
                throw new ArgumentNullException(nameof(messageHandler));

            var type = typeof(TMessage);

            lock (subscriptionsLock)
            {
                if (!subscriptions.ContainsKey(type))
                    subscriptions.Add(type, new List<ISubscription>());

                var token = new SubscriptionToken(type);
                subscriptions[type].Add(new Subscription<TMessage>(messageHandler, token));
                return token;
            }
        }

        public void Unsubscribe(SubscriptionToken token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            lock (subscriptionsLock)
            {
                if (subscriptions.ContainsKey(token.EventType))
                {
                    var allSubscriptions = subscriptions[token.EventType];
                    var subscriptionToRemove = allSubscriptions.FirstOrDefault(x => x.SubscriptionToken.Token == token.Token);
                    if (subscriptionToRemove != null)
                        subscriptions[token.EventType].Remove(subscriptionToRemove);
                }
            }
        }
    }
}