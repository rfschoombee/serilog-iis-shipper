using System;

namespace Serilog.Seq.Infrastructure
{
    public class Subscription<TMessage> : ISubscription where TMessage : IMessage
    {
        private readonly Action<TMessage> action;
        public SubscriptionToken SubscriptionToken { get; }

        public Subscription(Action<TMessage> action, SubscriptionToken token)
        {
            this.action = action ?? throw new ArgumentNullException(nameof(action));
            SubscriptionToken = token ?? throw new ArgumentNullException(nameof(token));
        }

        public void Publish(IMessage eventItem)
        {
            if (!(eventItem is TMessage @event))
                throw new ArgumentException("Invalid Event Type.");

            action.Invoke(@event);
        }
    }
}