using System;

namespace Serilog.Seq.Infrastructure
{
    public class SubscriptionToken
    {
        internal SubscriptionToken(Type eventItemType)
        {
            Token = Guid.NewGuid();
            EventType = eventItemType;
        }

        public Guid Token { get; }

        public Type EventType { get; }
    }

}