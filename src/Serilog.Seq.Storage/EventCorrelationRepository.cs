
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LiteDB;

namespace Serilog.Seq.Storage
{
    public class EventCorrelationReposiotry : IEventCorrelationRepository
    {
        private readonly ILiteDatabase dbContext;
        private readonly ILiteCollection<EventCorrelation> collection;
        private readonly ILogger logger;

        public EventCorrelationReposiotry(LiteDbContext dbContext, ILogger logger)
        {
            this.dbContext = dbContext.Context;
            this.collection = this.dbContext.GetCollection<EventCorrelation>("event_correlation");
            this.logger = logger;

            collection.EnsureIndex(o => o.Hash);
        }

        public async Task<IEnumerable<EventCorrelation>> GetEventsAsync()
        {
            return await ExecuteQueryAsync<IEnumerable<EventCorrelation>>(() => collection.FindAll());
        }

        public async Task AddEventAsync(EventCorrelation @event)
        {
            await ExecuteNonQueryAsync(() => collection.Insert(@event) != BsonValue.Null);
        }

        public async Task RemoveEventAsync(EventCorrelation @event)
        {
            await ExecuteNonQueryAsync(() => collection.Delete(@event.EventId));
        }

        private async Task<T> ExecuteQueryAsync<T>(Func<T> action)
        {
            return await Task.Run(() =>
            {
                try
                {
                    return action.Invoke();
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error executing repository task.");
                    return default;
                }
            });
        }
        private async Task<bool> ExecuteNonQueryAsync(Func<bool> action)
        {
            return await Task.Run(() =>
            {
                try
                {
                    return action.Invoke();
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error executing repository task.");
                    return false;
                }
            });
        }
    }
}