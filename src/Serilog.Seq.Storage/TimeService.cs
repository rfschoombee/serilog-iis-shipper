using Microsoft.Extensions.Hosting;
using Serilog.Seq.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Serilog.Seq.Storage
{
    public class TimeService : IHostedService
    {
        private readonly ILogger logger;
        private readonly IMessageBus messageBus;
        private readonly CancellationTokenSource cancellationTokenSource;
        private DateTime lastCheck;
        private Task task;


        public TimeService(ILogger logger, IMessageBus messageBus)
        {
            cancellationTokenSource = new CancellationTokenSource();
            lastCheck = DateTime.Now;
            this.logger = logger;
            this.messageBus = messageBus;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            task = Task.Run(() => Worker(), cancellationTokenSource.Token);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            cancellationTokenSource.Cancel();
            return Task.CompletedTask;
        }

        private async void Worker()
        {
            while (true)
            {
                if (DateTime.Now.Subtract(lastCheck).Days > 0)
                {
                    logger.Information($"Day has passed");
                    lastCheck = DateTime.Now;
                    DateTime passedDay = lastCheck.AddDays(-1);

                    await messageBus.PublishAsync(new DayHasPassedEvent(passedDay));
                }
                Thread.Sleep(10000);
            }
        }
    }
}