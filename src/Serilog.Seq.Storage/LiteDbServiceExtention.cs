﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Serilog.Seq.Storage
{
    public static class LiteDbServiceExtention
    {
        public static void AddLiteDb(this IServiceCollection services, HostBuilderContext hostContext)
        {
            services.Configure<LiteDatabaseConfiguration>(hostContext.Configuration.GetSection("LiteDatabase"));
            services.AddTransient<LiteDbContext, LiteDbContext>();
            services.AddTransient<IEventCorrelationRepository, EventCorrelationReposiotry>();
        }
    }
}