using System.Collections.Generic;
using System.Threading.Tasks;

namespace Serilog.Seq.Storage
{
    public interface IEventCorrelationRepository
    {
        Task<IEnumerable<EventCorrelation>> GetEventsAsync();
        Task AddEventAsync(EventCorrelation @event);
        Task RemoveEventAsync(EventCorrelation @event);
    }
}