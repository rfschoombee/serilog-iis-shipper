namespace Serilog.Seq.Storage
{
    public class LiteDatabaseConfiguration
    {
        public string DatabasePath { get; set; }
    }
}