using Serilog.Seq.Infrastructure;
using System;

namespace Serilog.Seq.Storage
{
    public class DayHasPassedEvent : IMessage
    {
        private readonly DateTime timestamp;

        public DayHasPassedEvent(DateTime timestamp)
        {
            this.timestamp = timestamp;
            Id = Guid.NewGuid();
        }
        public DateTime TimeStamp { get; set; }
        public Guid Id { get; set; }
    }
}