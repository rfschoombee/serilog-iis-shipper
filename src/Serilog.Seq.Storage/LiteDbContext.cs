using System;
using LiteDB;
using Microsoft.Extensions.Options;

namespace Serilog.Seq.Storage
{
    public class LiteDbContext
    {
        public readonly ILiteDatabase Context;
        public LiteDbContext(IOptions<LiteDatabaseConfiguration> options)
        {
            try
            {
                var database = new LiteDatabase(options.Value.DatabasePath);
                if (database != null)
                    Context = database;
            }
            catch (Exception ex)
            {
                throw new Exception("Can't find or create LiteDb database.", ex);
            }
        }
    }
}