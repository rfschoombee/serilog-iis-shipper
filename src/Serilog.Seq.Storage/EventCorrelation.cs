using LiteDB;

namespace Serilog.Seq.Storage
{
    public class EventCorrelation
    {
        public EventCorrelation()
        {

        }

        public EventCorrelation(string message)
        {
            Message = message;
            Hash = HashHelper.HashString(message);
        }

        public ObjectId EventId { get; set; }
        public string Message { get; set; }
        public string Hash { get; private set; }
    }
}